import { Injectable } from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) { }
    showSuccess(message:string, title:string, ToastConfig) {
      this.toastr.success(message, title, ToastConfig);
    }

    showError(message, title) {
      this.toastr.error(message, title);
    }

    showInfo(message, title) {
      this.toastr.info(message, title);
    }

    showWarning(message: string, title: string, ToastConfig) {
      this.toastr.warning(message, title, ToastConfig);
    }
}
