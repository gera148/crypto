import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';
import {Router} from '@angular/router';
import {faArrowRight} from '@fortawesome/free-solid-svg-icons';
import {NotificationService} from '../notification.service';


@Component({
  selector: 'app-portfolio',
  template: `
    <div class="container">
      <h1 class="title">Portfolio</h1>
      <p class="smallTitle">Here is your Portfolio. You can see here your crypto currencies.</p>
      <div *ngIf="empty" class="empty-holder">
        <img src="/assets/portfolio/empty.svg" alt="">
        <h2>Your portfolio is empty. Please buy crypto currencies.</h2>
      </div>
      <table *ngIf="!empty" class="content-table">
        <thead>
        <tr>
          <td class="trade" id="mid">TRADE</td>
          <td>NAME</td>
          <td class="amount">AMOUNT</td>
          <td class="buyEx">BUYED EXCHANGE RATE</td>
          <td class="currEx">CURRENT EXCHANGE RATE</td>
          <td class="buyPrice">BUYED PRICE</td>
          <td>CURRENT PRICE</td>
          <td>PROFIT</td>
          <td>DELETE</td>
        </tr>
        </thead>
        <tbody>
        <tr *ngFor="let buyedCrypto of buyedCryptos;let i = index">
          <td class="trade" id="mid">{{i+1}}.</td>
          <td class="important">{{buyedCrypto.name}}</td>
          <td class="amount">{{buyedCrypto.quantity}}</td>
          <td class="buyEx">$ {{buyedCrypto.price_usd}}</td>
          <td class="currEx">$ {{getActualCryptoPrice(buyedCrypto.id)}}</td>
          <td class="important buyPrice">$ {{buyedCrypto.quantity*buyedCrypto.price_usd | number: '1.0-3'}}</td>
          <td class="important">$ {{buyedCrypto.quantity * getActualCryptoPrice(buyedCrypto.id) | number: '1.0-3'}}</td>
          <td [style.color]="isColor((getActualCryptoPrice(buyedCrypto.id)-buyedCrypto.price_usd)*buyedCrypto.quantity) ? 'green' : 'red'">
            $ {{(getActualCryptoPrice(buyedCrypto.id)-buyedCrypto.price_usd)*buyedCrypto.quantity | number: '1.0-3'}}
          </td>
          <td>
            <button (click)="deleteBuy(buyedCrypto.id)" >DELETE</button>
          </td>
        </tr>
        </tbody>
      </table>

    </div>
  `,
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  public buyedCryptos = [];
  constructor(private service: DataService, private router: Router, private notifyService: NotificationService) { }
  public cryptosList:any;
  empty = false;
  faArrowRight = faArrowRight;
  ngOnInit(): void {
    this.service.getCryptos()
      .subscribe(
        (data) => {
          this.cryptosList = data;
        },
        (err) => console.error(err),
        () => console.log(this.cryptosList.data)
      );
    this.buyedCryptos = this.service.getBuyedCriptos();
    if (this.buyedCryptos == null || !this.buyedCryptos.length) { this.empty = true; }
  }
  showSuccess(): void {
    this.notifyService.showSuccess("The deletion was successful!", "Success!", { positionClass: "toast-top-full-width"});
  }
  getActualCryptoPrice(id) {
    const searchedPrice = this.cryptosList.data.find(x => x.id === id);
    return(searchedPrice.price_usd);
  }
  deleteBuy(id) {
    const deleteIndex = this.buyedCryptos.findIndex(x => x.id === id);
    this.buyedCryptos.splice(deleteIndex,1);
    localStorage.removeItem('buyed');
    localStorage.setItem('buyed', JSON.stringify(this.buyedCryptos));
    this.showSuccess();
  }
  isColor(value) {
    if (value > 0) { return true; }
    return false;
  }

}
